# TODO
+ !arnie !list, !arnie !add and !arnie !remove
+ !arnie !version
- Parse arnie's commands in a more sophisticated way, allowing more whitespace,
  different case etc.
- !goldstar command
- Stop using Redis types for errors
- Debug store_passphrase in Redis crypto store - it didn't work when I tried
- Create a Redis crypto store
 + Implementation
 - Address PR comments
 - Move into own repo
- Allow arnie to greet you on specific days at somewhat random times
- !todo command
- Permissions to add and remove arnie quotes
