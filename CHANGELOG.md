# Changelog for Arnie

## [0.5.0] - 2023-05-22

### Added

* `!arnie !list` to show all of Arnie's quotes
* `!arnie !remove` to remove a quote
* `!arnie !add` to add a quote
* `!arnie !version` to give version info
