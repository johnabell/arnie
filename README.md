# Arnie

Arnie is a Matrix bot.

## Development

To set up:

* [Install rust](https://www.rust-lang.org/tools/install)
* Install prerequisites:
```sh
rustup component add rustfmt
```

Then, to compile and run tests:

```sh
cargo test
```

To compile a release version:

```sh
cargo build --release
```

Or to run:

```sh
$ ARNIE_DB_PREFIX="arniebot|" \
  ARNIE_REDIS_URL="redis://localhost:6379" \
  ARNIE_MATRIX_HOMESERVER_URL="http://localhost:8008" \
  ARNIE_MATRIX_USERNAME="myusername" \
  ARNIE_MATRIX_PASSWORD="mypassword" \
  cargo run
>>> !arnie
Get out.
```

Alternatively, if you would like to store the matrix credentials in Redis, set
the following fields in Redis. Then you can omit the ARNIE_MATRIX variables from
the environment. If both are supplied, the values provided via environment
variables are used.

```sh
redis-cli
SET "arniebot|matrix_sender_receiver|homeserver_url" "http://localhost:8008"
SET "arniebot|matrix_sender_receiver|username" "myusername"
SET "arniebot|matrix_sender_receiver|password" "mypassword"
```

## Dockerizing Arnie

We built him inside a Docker container: rust:buster using:

```sh
cargo build --release
```

Then built his own container using his binary with the dockerfile "Dockerfile-arm64-rust-buster"

You don't need to do it this way, just as long as you know here his binary is.

This dockerfile an entrypoint that connects to a redis server in K8S that is in a different namespace:

```sh
arnie -d "arniebot|" -r redis://redis.redis.svc.cluster.local
```

You can of course run everything in the same namespace if you so wish.

Push that container to a repo, then we are ready to use a Helm chart to deploy him.

## Deploying Arnie using Helm

A sample Helm chart is included.

In templates/arnie.yaml RUST_BACKTRACE is set to full for logging purposes, this can be removed if not wanted.

In templates/redis-service.yaml is an example of how to connect to a redis in a different namespace.

In Chart.yaml, the appVersion should be changed to reflect the version of Arnie that has been built.

In values.yaml, change the image value should be changed to reflect your repo and image name.

## Releases

To make a release:

* Update `Cargo.toml` changing the version number
* Run `cargo test` to generate `Cargo.lock`
* Commit the changes and push
* Create the tag e.g. `git tag 0.3.1 && git push --tags`

## License

Copyright 2019-2023 Andy Balaam and the Arnie contributors.

Released under the AGPLv3 license. See [LICENSE](LICENSE) for info.

## Code of conduct

Please note that this project is released with a
[Contributor Code of Conduct](code_of_conduct.md). By participating in this
project you agree to abide by its terms.

[![Contributor Covenant](contributor-covenant-v2.0-adopted-ff69b4.svg)](code_of_conduct.md)
