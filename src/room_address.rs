#[derive(Clone, Debug, Eq, PartialEq)]
pub enum RoomAddress {
    Admin,
    CommandLine,
    Matrix(String), // room_id
}
