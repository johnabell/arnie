/*use crate::chooser::Chooser;
use crate::cmdrun::CmdRun;
use crate::types::command::*;
use crate::types::notification::{Channel, Notification};
use crate::types::text_content::TextContent;

pub fn process_text<C: Chooser>(
    cmdrun: &mut dyn CmdRun,
    chooser: &mut C,
    content: &TextContent,
    notif: &Notification,
) -> Vec<Command> {
    if let Some(ans) = arnie(chooser, content, notif) {
        ans
    } else if let Some(ans) = caps_arnie(chooser, content, notif) {
        ans
    } else if let Some(ans) = task(cmdrun, content, notif) {
        ans
    } else if let Some(ans) = awight(chooser, content, notif) {
        ans
    } else {
        vec![]
    }
}

const ARNIE_QUOTES: [&str; 29] = [
    "I need a vacation.",
    "Let off some steam, Bennett.",
    "You’ve just been erased.",
    "Get your ass to Mars!",
    "It's not a toomah!",
    "No problemo.",
    "Screw you!",
    "Chill out, d___wad.",
    "Come with me if you want to live.",
    "If it bleeds, we can kill it.",
    "Do it. Do it now!",
    "See you at the party, Richter!",
    "Get to da choppa!",
    "Hasta la vista, baby.",
    "Consider that a divorce.",
    "I'll be back.",
    "To crush your enemies, see dem driven before you, and to hear de lamentation of der women.",
    "Remember when I said I'd kill you last?  I lied.",
    "You should not drink, and bake.",
    "CROM",
    "You got what you want Cohagen, give dese people air!",
    "Don't disturb my friend, he's dead tired.",
    "You blew my cover!",
    "I am not Quaid!",
    "You think this is the real Quaid? It is!",
    "Sleazy...Demure.",
    "You're Luggage.",
    "Mimetic polyalloy",
    "Sarah Connor?",
];

fn arnie_build_quote<'a, C: Chooser>(
    chooser: &mut C,
    provided_quote: &'a str,
) -> &'a str {
    if provided_quote.is_empty() {
        chooser.choose(&ARNIE_QUOTES).unwrap_or(&"")
    } else {
        provided_quote
    }
}

fn arnie<C: Chooser>(
    chooser: &mut C,
    content: &TextContent,
    notif: &Notification,
) -> Option<Vec<Command>> {
    if content.text.body.starts_with("!arnie") {
        let quote = arnie_build_quote(chooser, content.text.body[6..].trim());
        Some(send(format!("*{}*", quote), &notif.msg.channel))
    } else {
        None
    }
}

fn caps_arnie<C: Chooser>(
    chooser: &mut C,
    content: &TextContent,
    notif: &Notification,
) -> Option<Vec<Command>> {
    if content.text.body.starts_with("!ARNIE") {
        let quote = arnie_build_quote(chooser, content.text.body[6..].trim());
        Some(send(
            format!("*_{}_*", quote.to_ascii_uppercase()),
            &notif.msg.channel,
        ))
    } else {
        None
    }
}

fn task(
    cmdrun: &mut dyn CmdRun,
    content: &TextContent,
    notif: &Notification,
) -> Option<Vec<Command>> {
    if content.text.body.starts_with("!task") {
        let sync_out = cmdrun.run("task", &vec!["sync"]);
        match sync_out {
            Ok(_response) => {
                let task_out = cmdrun.run("task", &vec![]);
                match task_out {
                    Ok(tasks) => Some(send(
                        format!("```{}```", tasks.trim().to_string()),
                        &notif.msg.channel,
                    )),
                    Err(e) => Some(send(
                        String::from(format!("Error: {}", e)),
                        &notif.msg.channel,
                    )),
                }
            }
            Err(e) => Some(send(
                String::from(format!("Error: {}", e)),
                &notif.msg.channel,
            )),
        }
    } else {
        None
    }
}

fn awight<C: Chooser>(
    chooser: &mut C,
    content: &TextContent,
    notif: &Notification,
) -> Option<Vec<Command>> {
    let msg = content.text.body.to_lowercase();
    if msg.starts_with("awight second")
        || msg.starts_with("awight, second")
        || msg.starts_with("awight lads second")
        || msg.starts_with("awight lads, second")
    {
        let res = chooser
            .choose(&[
                "Awight thirds",
                "awight, thirds-aaa",
                "Awight, thirds",
                "Awight lads, thirds",
            ])
            .unwrap_or(&"");

        Some(send(String::from(*res), &notif.msg.channel))
    } else {
        None
    }
}

fn send(body: String, channel: &Channel) -> Vec<Command> {
    vec![Command {
        method: String::from("send"),
        params: Params {
            options: Options {
                channel: CommandChannel {
                    name: channel.name.clone(),
                    members_type: channel.members_type.clone(),
                },
                message: Message { body },
            },
        },
    }]
}

#[cfg(test)]
mod tests {
    use super::process_text;
    use crate::chooser::Chooser;
    use crate::cmdrun::CmdRun;
    use crate::types::command::*;
    use crate::types::notification::*;
    use crate::types::text_content::*;
    use std::io;

    #[test]
    fn random_writing_does_nothing() {
        let mut cmdrun = FakeCmdRun::new_dontuse();
        let mut rng = DontUseChooser;
        let content = txt("foo !arnie bar");
        assert_eq!(
            process_text(
                &mut cmdrun,
                &mut rng,
                &content,
                &notf("ch", None, &content)
            ),
            vec![]
        )
    }

    #[test]
    fn arnie_with_message_repeats_it_bold() {
        let mut cmdrun = FakeCmdRun::new_dontuse();
        let mut rng = DontUseChooser;
        let content = txt("!arnie foo");
        assert_eq!(
            process_text(
                &mut cmdrun,
                &mut rng,
                &content,
                &notf("ch", None, &content)
            ),
            vec![say("*foo*", "ch", None)]
        )
    }

    #[test]
    fn arnie_to_person_with_message_repeats_it_bold() {
        let mut cmdrun = FakeCmdRun::new_dontuse();
        let mut rng = DontUseChooser;
        let content = txt("!arnie foo");
        assert_eq!(
            process_text(
                &mut cmdrun,
                &mut rng,
                &content,
                &notf("ch", None, &content)
            ),
            vec![say("*foo*", "ch", None)]
        )
    }

    #[test]
    fn arnie_to_team_with_message_repeats_it_bold() {
        let mut cmdrun = FakeCmdRun::new_dontuse();
        let mut rng = DontUseChooser;
        let content = txt("!arnie foo");
        assert_eq!(
            process_text(
                &mut cmdrun,
                &mut rng,
                &content,
                &notf("ch", Some("team"), &content)
            ),
            vec![say("*foo*", "ch", Some("team"))]
        )
    }

    #[test]
    fn arnie_with_no_args_says_a_random_quote() {
        let mut cmdrun = FakeCmdRun::new_dontuse();
        let mut rng = FakeChooser::new(1);
        let content = txt("!arnie");
        assert_eq!(
            process_text(
                &mut cmdrun,
                &mut rng,
                &content,
                &notf("ch", Some("team"), &content)
            ),
            vec![say("*Let off some steam, Bennett.*", "ch", Some("team"))]
        )
    }

    #[test]
    fn caps_arnie_really_shouts_it() {
        let mut cmdrun = FakeCmdRun::new_dontuse();
        let mut rng = DontUseChooser;
        let content = txt("!ARNIE get to da choppa!");
        assert_eq!(
            process_text(
                &mut cmdrun,
                &mut rng,
                &content,
                &notf("ch", Some("team"), &content)
            ),
            vec![say("*_GET TO DA CHOPPA!_*", "ch", Some("team"))]
        )
    }

    #[test]
    fn arnie_with_no_args_shouts_a_random_quote() {
        let mut cmdrun = FakeCmdRun::new_dontuse();
        let mut rng = FakeChooser::new(2);
        let content = txt("!ARNIE");
        assert_eq!(
            process_text(
                &mut cmdrun,
                &mut rng,
                &content,
                &notf("ch", Some("team"), &content)
            ),
            vec![say("*_YOU’VE JUST BEEN ERASED._*", "ch", Some("team"))]
        )
    }

    #[test]
    fn can_ask_what_tasks_are_active() {
        let mut cmdrun = FakeCmdRun::new2(
            "task",
            &vec![String::from("sync")],
            "Synced!",
            "task",
            &vec![],
            "task 1\ntask 2\n",
        );
        let mut rng = DontUseChooser;
        let content = txt("!task");
        assert_eq!(
            process_text(
                &mut cmdrun,
                &mut rng,
                &content,
                &notf("ch", None, &content)
            ),
            vec![say("```task 1\ntask 2```", "ch", None)]
        )
    }

    struct DontUseChooser;

    impl Chooser for DontUseChooser {
        fn choose<'a>(&self, _array: &'a [&str]) -> Option<&&'a str> {
            None
        }
    }

    struct FakeChooser(usize);

    impl FakeChooser {
        fn new(n: usize) -> FakeChooser {
            FakeChooser(n)
        }
    }

    impl Chooser for FakeChooser {
        fn choose<'a>(&self, array: &'a [&str]) -> Option<&&'a str> {
            array.get(self.0)
        }
    }

    struct FakeCmdRun {
        cmd: Vec<String>,
        args: Vec<Vec<String>>,
        output: Vec<String>,
        counter: usize,
    }

    impl FakeCmdRun {
        fn new_dontuse() -> FakeCmdRun {
            FakeCmdRun {
                cmd: vec![],
                args: vec![],
                output: vec![],
                counter: 0,
            }
        }
        fn new2(
            cmd1: &str,
            args1: &Vec<String>,
            output1: &str,
            cmd2: &str,
            args2: &Vec<String>,
            output2: &str,
        ) -> FakeCmdRun {
            FakeCmdRun {
                cmd: vec![String::from(cmd1), String::from(cmd2)],
                args: vec![args1.clone(), args2.clone()],
                output: vec![String::from(output1), String::from(output2)],
                counter: 0,
            }
        }
    }

    impl CmdRun for FakeCmdRun {
        fn run(&mut self, cmd: &str, args: &Vec<&str>) -> io::Result<String> {
            assert_eq!(cmd, self.cmd[self.counter]);
            assert_eq!(
                args.join("|"), // TODO - not good
                self.args[self.counter].join("|")
            );
            self.counter = self.counter + 1;
            Ok(self.output[self.counter - 1].clone())
        }
    }

    fn say(
        body: &str,
        channel_name: &str,
        members_type: Option<&str>,
    ) -> Command {
        Command {
            method: String::from("send"),
            params: Params {
                options: Options {
                    channel: CommandChannel {
                        name: String::from(channel_name),
                        members_type: String::from(
                            members_type.unwrap_or("impteamnative"),
                        ),
                    },
                    message: Message {
                        body: String::from(body),
                    },
                },
            },
        }
    }

    fn txt(body: &str) -> TextContent {
        TextContent {
            text: Text {
                body: String::from(body),
                payments: None,
            },
        }
    }

    fn notf(
        channel_name: &str,
        members_type: Option<&str>,
        content: &TextContent,
    ) -> Notification {
        Notification {
            type_: String::from("chat"),
            source: String::from("remote"),
            msg: Msg {
                id: 12345,
                channel: Channel {
                    name: String::from(channel_name),
                    members_type: String::from(
                        members_type.unwrap_or("impteamnative"),
                    ),
                    topic_type: String::from("chat"),
                    topic_name: Some(String::from("general")),
                },
                sender: Sender {
                    uid: String::from("a1b2c3"),
                    username: String::from("myuser"),
                    device_id: String::from("aaaa0000"),
                    device_name: String::from("mydevice"),
                },
                sent_at: 324235,
                sent_at_ms: 324235000,
                content: Content::TextContent(content.clone()),
                prev: None,
                unread: true,
                at_mention_usernames: None,
                channel_mention: String::from("none"),
            },
            pagination: Pagination {
                next: String::from("02"),
                previous: String::from("00"),
                num: 1,
                last: false,
            },
        }
    }
}*/
