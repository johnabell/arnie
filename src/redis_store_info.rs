pub struct RedisStoreInfo {
    /// The URL on which to find a Redis instance
    pub redis_url: String,

    /// The key prefix to use inside Redis for this crypto store
    pub store_prefix: String,
}
