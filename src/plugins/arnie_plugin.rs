use async_trait::async_trait;
use rand::rngs::SmallRng;
use rand::Rng;
use rand::SeedableRng;
use redis::RedisResult;

use crate::args::VERSION;
use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::message::Message;
use crate::plugins::plugin::Plugin;

const SCHEMA_VERSION: usize = 1;
const QUOTES_KEY: &str = "quotes";

const DEFAULT_QUOTES: [&str; 29] = [
    "I need a vacation.",
    "Let off some steam, Bennett.",
    "You’ve just been erased.",
    "Get your ass to Mars!",
    "It's not a toomah!",
    "No problemo.",
    "Screw you!",
    "Chill out, d___wad.",
    "Come with me if you want to live.",
    "If it bleeds, we can kill it.",
    "Do it. Do it now!",
    "See you at the party, Richter!",
    "Get to da choppa!",
    "Hasta la vista, baby.",
    "Consider that a divorce.",
    "I'll be back.",
    "To crush your enemies, see dem driven before you, and to hear de \
    lamentation of der women.",
    "Remember when I said I'd kill you last?  I lied.",
    "You should not drink, and bake.",
    "CROM",
    "You got what you want Cohagen, give dese people air!",
    "Don't disturb my friend, he's dead tired.",
    "You blew my cover!",
    "I am not Quaid!",
    "You think this is the real Quaid? It is!",
    "Sleazy...Demure.",
    "You're Luggage.",
    "Mimetic polyalloy",
    "Sarah Connor?",
];

pub struct VersionInfo {
    app_version: &'static str,
    schema_version: usize,
}

impl VersionInfo {
    pub fn new(app_version: &'static str, schema_version: usize) -> Self {
        Self {
            app_version,
            schema_version,
        }
    }

    /// Gather the real version info and store it
    fn real() -> Self {
        Self {
            app_version: VERSION,
            schema_version: SCHEMA_VERSION,
        }
    }

    fn format(&self) -> String {
        format!(
            "arnie version: {app_version}\narnieplugin schema_version: {schema_version}",
            app_version=self.app_version,
            schema_version=self.schema_version,
        )
    }
}

pub struct ArniePlugin<Db: Database> {
    db: DatabaseWrapper<Db>,
    rng: SmallRng,
    version_info: VersionInfo,
}

impl<Db: Database> ArniePlugin<Db> {
    pub async fn new(
        db: DatabaseWrapper<Db>,
        random_seed: u64,
    ) -> RedisResult<Self> {
        Self::new_with_version(db, random_seed, VersionInfo::real()).await
    }

    pub async fn new_with_version(
        mut db: DatabaseWrapper<Db>,
        random_seed: u64,
        version_info: VersionInfo,
    ) -> RedisResult<Self> {
        if !db.exists("installed").await? {
            db.set_usize("installed", SCHEMA_VERSION).await?;
            for quote in DEFAULT_QUOTES {
                db.lpush(QUOTES_KEY, quote).await?;
            }
        }

        Ok(Self {
            db,
            rng: SmallRng::seed_from_u64(random_seed),
            version_info,
        })
    }

    async fn random_quote(&mut self) -> String {
        // TODO: cache quotes
        let num_quotes = self
            .db
            .llen(QUOTES_KEY)
            .await
            .expect("Failed to find quotes!");
        let index = self.rng.gen_range(0..num_quotes);

        self.db
            .lindex(
                QUOTES_KEY,
                isize::try_from(index)
                    .expect("Quote index was unexpectedly large!"),
            ) // TODO: make quotes a const
            .await
            .expect("Failed to get quote!")
            .expect("Quote is missing")
    }

    async fn list_quotes(&self) -> String {
        let quotes: Vec<String> = self
            .db
            .lrange(QUOTES_KEY, 0, -1)
            .await
            .expect("Failed to get quotes!")
            .iter()
            .enumerate()
            .map(|(i, s)| format!("[{}] {}", i, s))
            .collect();
        quotes.join("\n")
    }

    async fn remove_quote(&mut self, index: isize) -> Result<(), String> {
        let index = isize::try_from(index)
            .map_err(|_| String::from("Error: index was too big"))?;

        let quotes_len = self.db.llen(QUOTES_KEY).await.map_err(|_| {
            String::from("Error: failed to find number of quotes")
        })?;

        if index < 0 {
            return Err(format!(
                "Error: negative numbers are not allowed - \
                    try a number 0-{} inclusive.",
                quotes_len - 1
            ));
        } else if index >= quotes_len as isize {
            return Err(format!(
                "Error: number too large - \
                    try a number 0-{} inclusive.",
                quotes_len - 1
            ));
        }

        let quote = self
            .db
            .lindex(QUOTES_KEY, index)
            .await
            .map_err(|_| String::from("Error: couldn't find quote."))?;
        if let Some(quote) = quote {
            self.db
                .lrem(QUOTES_KEY, 1, &quote)
                .await
                .map_err(|_| String::from("Error: failed to remove quote."))
        } else {
            Ok(())
        }
    }

    /// Deletes the quote with index index. (Note: also deletes any other
    /// quotes with value '__deleted__'.)
    pub async fn delete_quote(&mut self, index: isize) -> RedisResult<()> {
        self.db.lset(QUOTES_KEY, index, "__deleted__").await?;
        self.db.lrem(QUOTES_KEY, 0, "__deleted__").await
    }

    async fn run_remove(&mut self, text: &str) -> String {
        let command_len = "!arnie !remove ".len();
        let args_str = &text[command_len..];
        let index = args_str.parse();
        if let Ok(index) = index {
            let res = self.remove_quote(index).await;
            match res {
                Ok(()) => format!("Quote {} removed.", index),
                Err(e) => e,
            }
        } else {
            String::from("Error: not a number - try e.g. !arnie !remove 3")
        }
    }

    async fn run_add(&mut self, text: &str) -> String {
        let command_len = "!arnie !add ".len();
        let args_str = &text[command_len..];
        let res = self.db.lpush(QUOTES_KEY, args_str).await;
        if let Ok(_) = res {
            String::from("Quote added.")
        } else {
            String::from("Error: failed to add quote.")
        }
    }

    async fn run_version(&mut self) -> String {
        self.version_info.format()
    }
}

#[async_trait]
impl<Db: Database + Send> Plugin for ArniePlugin<Db> {
    async fn message(
        &mut self,
        message: &Message,
    ) -> RedisResult<Option<Message>> {
        if !message.text.starts_with("!arnie") {
            return Ok(None);
        }

        let quote = if message.text.starts_with("!arnie !list") {
            self.list_quotes().await
        } else if message.text.starts_with("!arnie !remove ") {
            self.run_remove(&message.text).await
        } else if message.text.starts_with("!arnie !add ") {
            self.run_add(&message.text).await
        } else if message.text == "!arnie !version" {
            self.run_version().await
        } else if message.text.trim().len() > "!arnie ".len() {
            String::from(&message.text["!arnie ".len()..])
        } else {
            self.random_quote().await
        };

        Ok(Some(Message::new(quote, message.room_address.clone())))
    }
}

#[cfg(test)]
mod test {
    use std::sync::Arc;
    use tokio::sync::Mutex;

    use crate::{memory_database::MemoryDatabase, room_address::RoomAddress};

    use super::*;

    #[tokio::test]
    async fn arnie_says_different_things_every_time() {
        // Given an Arnie plugin
        let mut arnie = new_arnie().await;

        // When I ask for two messages, they are different
        assert_eq!(
            &quote_response(&mut arnie).await,
            "Consider that a divorce."
        );

        assert_eq!(&quote_response(&mut arnie).await, "It's not a toomah!");
    }

    #[tokio::test]
    async fn arnie_list_says_all_the_quotes() {
        // Given an Arnie plugin with a small list of quotes
        let mut arnie = new_3_quote_arnie().await;

        // When I ask for two messages, they are different
        assert_eq!(
            &command_response(&mut arnie, "!list").await,
            "\
            [0] You’ve just been erased.\n\
            [1] Let off some steam, Bennett.\n\
            [2] I need a vacation.\
            "
        );
    }

    #[tokio::test]
    async fn can_remove_first_quote_from_arnie() {
        // Given an Arnie plugin with a small list of quotes
        let mut arnie = new_3_quote_arnie().await;

        // When I remove the first quote
        assert_eq!(
            &command_response(&mut arnie, "!remove 0").await,
            "Quote 0 removed."
        );

        // Then it is gone
        assert_eq!(
            &command_response(&mut arnie, "!list").await,
            "\
            [0] Let off some steam, Bennett.\n\
            [1] I need a vacation.\
            "
        );
    }

    #[tokio::test]
    async fn can_remove_quotes_from_arnie() {
        // Given an Arnie plugin with lots of quoted
        let mut arnie = new_arnie().await;

        // When I remove a quote with a large index
        assert_eq!(
            &command_response(&mut arnie, "!remove 15").await,
            "Quote 15 removed."
        );

        // And then remove all but two quotes
        for _ in 0..(DEFAULT_QUOTES.len() - 3) {
            assert_eq!(
                &command_response(&mut arnie, "!remove 1").await,
                "Quote 1 removed."
            );
        }

        // Then only 2 are left
        assert_eq!(
            &command_response(&mut arnie, "!list").await,
            "\
            [0] Sarah Connor?\n\
            [1] I need a vacation.\
            "
        );
    }

    #[tokio::test]
    async fn can_remove_last_quote_from_arnie() {
        // Given an Arnie plugin with a small list of quotes
        let mut arnie = new_3_quote_arnie().await;

        // When I remove a quote
        assert_eq!(
            &command_response(&mut arnie, "!remove 2").await,
            "Quote 2 removed."
        );

        // Then it is gone
        assert_eq!(
            &command_response(&mut arnie, "!list").await,
            "\
            [0] You’ve just been erased.\n\
            [1] Let off some steam, Bennett.\
            "
        );
    }

    #[tokio::test]
    async fn arnie_remove_with_nonnumeric_index_is_an_error() {
        // Given an Arnie plugin
        let mut arnie = new_arnie().await;

        // When I remove a quote using a bad index, I see an error
        assert_eq!(
            &command_response(&mut arnie, "!remove Foo").await,
            "Error: not a number - try e.g. !arnie !remove 3"
        );
    }

    #[tokio::test]
    async fn arnie_remove_with_wrong_index_is_an_error() {
        // Given an Arnie plugin
        let mut arnie = new_arnie().await;

        // When I remove a quote using a negative index, I see an error
        assert_eq!(
            &command_response(&mut arnie, "!remove -5").await,
            &format!(
                "Error: negative numbers are not allowed - \
                try a number 0-{} inclusive.",
                DEFAULT_QUOTES.len() - 1
            )
        );

        // When I remove a quote using a too-large index, I see an error
        assert_eq!(
            &command_response(&mut arnie, "!remove 500").await,
            &format!(
                "Error: number too large - try a number 0-{} inclusive.",
                DEFAULT_QUOTES.len() - 1
            )
        );
    }

    #[tokio::test]
    async fn can_add_a_new_quote() {
        // Given an Arnie plugin with a small list of quotes
        let mut arnie = new_3_quote_arnie().await;

        // When I add a new quote
        assert_eq!(
            &command_response(&mut arnie, "!add This is A !NEW quote!").await,
            "Quote added."
        );

        // Then it appears
        assert_eq!(
            &command_response(&mut arnie, "!list").await,
            "\
            [0] This is A !NEW quote!\n\
            [1] You’ve just been erased.\n\
            [2] Let off some steam, Bennett.\n\
            [3] I need a vacation.\
            "
        );
    }

    #[tokio::test]
    async fn can_report_version() {
        // Given an Arnie plugin
        let mut arnie =
            new_arnie_with_version(VersionInfo::new("2.31.2", 99)).await;

        // When I ask for the version, he tells me
        assert_eq!(
            &command_response(&mut arnie, "!version").await,
            "\
            arnie version: 2.31.2\n\
            arnieplugin schema_version: 99\
            "
        );
    }

    #[tokio::test]
    async fn arnie_doesnt_repopulate_his_quotes_if_already_installed() {
        let underlying_db = Arc::new(Mutex::new(MemoryDatabase::new()));
        let db = DatabaseWrapper::new(
            underlying_db.clone(),
            String::from("test_prefix"),
        );

        // Install arnie into a database
        let mut arnie = ArniePlugin::new(db, 3).await.unwrap();

        // Delete all his quotes except the first one
        for i in (1..DEFAULT_QUOTES.len()).rev() {
            arnie.delete_quote(i as isize).await.unwrap();
        }

        // Sanity: he now only ever says one thing
        assert_eq!(&quote_response(&mut arnie).await, "Sarah Connor?");
        assert_eq!(&quote_response(&mut arnie).await, "Sarah Connor?");
        assert_eq!(&quote_response(&mut arnie).await, "Sarah Connor?");

        // Create another instance of arnie on the same same database
        let db =
            DatabaseWrapper::new(underlying_db, String::from("test_prefix"));
        let mut arnie = ArniePlugin::new(db, 3).await.unwrap();

        // Check that he didn't recreate all the quotes
        assert_eq!(&quote_response(&mut arnie).await, "Sarah Connor?");
        assert_eq!(&quote_response(&mut arnie).await, "Sarah Connor?");
        assert_eq!(&quote_response(&mut arnie).await, "Sarah Connor?");
    }

    async fn quote_response(arnie: &mut ArniePlugin<MemoryDatabase>) -> String {
        arnie
            .message(&Message::new(
                "!arnie".to_owned(),
                RoomAddress::CommandLine,
            ))
            .await
            .unwrap()
            .unwrap()
            .text
    }

    async fn command_response(
        arnie: &mut ArniePlugin<MemoryDatabase>,
        cmd: &str,
    ) -> String {
        arnie
            .message(&Message::new(
                format!("!arnie {}", cmd),
                RoomAddress::CommandLine,
            ))
            .await
            .unwrap()
            .unwrap()
            .text
    }

    async fn new_arnie() -> ArniePlugin<MemoryDatabase> {
        let db = DatabaseWrapper::new(
            Arc::new(Mutex::new(MemoryDatabase::new())),
            String::from("test_prefix"),
        );
        ArniePlugin::new(db, 3).await.unwrap()
    }

    async fn new_arnie_with_version(
        version_info: VersionInfo,
    ) -> ArniePlugin<MemoryDatabase> {
        let db = DatabaseWrapper::new(
            Arc::new(Mutex::new(MemoryDatabase::new())),
            String::from("test_prefix"),
        );
        ArniePlugin::new_with_version(db, 3, version_info)
            .await
            .unwrap()
    }

    async fn new_3_quote_arnie() -> ArniePlugin<MemoryDatabase> {
        let mut arnie = new_arnie().await;
        for _ in 0..(DEFAULT_QUOTES.len() - 3) {
            arnie.delete_quote(0).await.unwrap();
        }
        arnie
    }
}
