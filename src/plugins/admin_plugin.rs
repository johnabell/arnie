use async_trait::async_trait;
use redis::RedisResult;

use crate::{message::Message, room_address::RoomAddress};

#[async_trait]
pub trait AdminPlugin {
    async fn message(
        &mut self,
        message: &Message,
    ) -> RedisResult<AdminResponse>;
}

pub enum AdminAction {
    None,
    Error(String, String),
    Exit(i32),
}

pub struct AdminResponse {
    pub action: AdminAction,
    pub message: Option<Message>,
}

impl AdminResponse {
    pub fn none() -> Self {
        Self {
            action: AdminAction::None,
            message: None,
        }
    }

    pub fn error(message: &str, module: &str, error_message: &str) -> Self {
        Self {
            action: AdminAction::Error(
                String::from(module),
                String::from(error_message),
            ),
            message: Some(Message::new(message.to_owned(), RoomAddress::Admin)),
        }
    }

    pub fn exit(message: &str, exit_code: i32) -> Self {
        Self {
            action: AdminAction::Exit(exit_code),
            message: Some(Message::new(message.to_owned(), RoomAddress::Admin)),
        }
    }
}
