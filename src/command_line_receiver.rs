use async_trait::async_trait;
use tokio::io::{AsyncBufReadExt, AsyncRead, BufReader};
use tokio::sync::mpsc;

use crate::message::Message;
use crate::message_receiver::MessageReceiver;
use crate::room_address::RoomAddress;

pub struct CommandLineReceiver<In: AsyncRead + Send + Unpin> {
    stdin: BufReader<In>,
}

impl<In: AsyncRead + Send + Unpin> CommandLineReceiver<In> {
    pub fn new(stdin: In) -> Self {
        Self {
            stdin: BufReader::new(stdin),
        }
    }
}

#[async_trait]
impl<In: AsyncRead + Send + Unpin> MessageReceiver for CommandLineReceiver<In> {
    async fn receive_forever(self, sender: mpsc::Sender<Message>) {
        let mut lines = self.stdin.lines();
        loop {
            let line = lines.next_line().await;
            match line {
                Err(_e) => {
                    // TODO: print error
                    break;
                }
                Ok(None) => {
                    // TODO: print message
                    break;
                }
                Ok(Some(line)) => {
                    sender
                        .send(Message::new(line, RoomAddress::CommandLine))
                        .await
                        .expect(
                            "Unable to pass message to bot from command \
                            line receiver",
                        );
                }
            }
        }
    }
}
