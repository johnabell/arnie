/*use rand::seq::SliceRandom;
use serde_json;
use std::io;

use crate::chooser::Chooser;
use crate::config::Config;
use crate::process_system::process_system;
use crate::process_text::process_text;
use crate::realcmdrun::RealCmdRun;
use crate::types::notification::{Content, Notification, Sender};

struct RngChooser;

impl Chooser for RngChooser {
    fn choose<'a>(&self, array: &'a [&str]) -> Option<&&'a str> {
        array.choose(&mut rand::thread_rng())
    }
}

pub fn process(cfg: &Config, inp: String) -> io::Result<String> {
    let notif: Notification = serde_json::from_str(&inp)?;

    if person_is_known(cfg, &notif.msg.sender) {
        let n2 = notif.clone();
        let cmds = match notif.msg.content {
            Content::SystemContent(sys) => process_system(sys),
            Content::TextContent(txt) => {
                process_text(&mut RealCmdRun::new(), &mut RngChooser, &txt, &n2)
            }
        };

        let mut ret: Vec<String> = vec![];
        for cmd in cmds {
            ret.push(serde_json::to_string(&cmd)?)
        }
        Ok(ret.join("\n"))
    } else {
        Ok(String::from(""))
    }
}

fn person_is_known(cfg: &Config, sender: &Sender) -> bool {
    cfg.known_usernames.contains(&sender.username)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::types::notification::*;
    use crate::types::system_content::*;
    use crate::types::text_content::*;

    #[test]
    fn non_json_fails() {
        assert!(process(&cfg(), String::from("foo")).is_err());
    }

    #[test]
    fn json_not_matching_expected_fails() {
        assert!(process(&cfg(), String::from("{}")).is_err());
    }

    #[test]
    fn json_addedtoteam_is_allowed() {
        let raw_json_msg = r#"{"type":"chat","source":"remote","msg":{"id":3,"channel":{"name":"arnie_and_me","members_type":"team","topic_type":"chat","topic_name":"general"},"sender":{"uid":"bbbb1111bbbb1111bbbb1111bbbb1111","username":"andybalaam","device_id":"aaaa0000aaaa0000aaaa0000aaaa0000","device_name":"Home"},"sent_at":1550097218,"sent_at_ms":1550097218197,"content":{"type":"system","system":{"systemType":0,"addedtoteam":{"team":"arnie_and_me","adder":"andybalaam","addee":"arnie","owners":["andybalaam"],"admins":null,"writers":null,"readers":["arnie"]}}},"prev":null,"unread":false,"at_mention_usernames":["arnie"],"channel_mention":"none"},"pagination":{"next":"03","previous":"03","num":1,"last":false}}"#;

        let notif: Notification = serde_json::from_str(&raw_json_msg).unwrap();

        assert_eq!(notif.msg.id, 3);
        assert_eq!(
            notif.msg.content.assert_system().system.addedtoteam.team,
            "arnie_and_me"
        );
    }

    #[test]
    fn json_text_from_team_is_allowed() {
        let raw_json_msg = r#"{"type":"chat","source":"remote","msg":{"id":4,"channel":{"name":"arnie_and_me","public":false,"members_type":"team","topic_type":"chat","topic_name":"general"},"sender":{"uid":"bbbb1111bbbb1111bbbb1111bbbb1111","username":"andybalaam","device_id":"aaaa0000aaaa0000aaaa0000aaaa0000","device_name":"Home"},"sent_at":1550097242,"sent_at_ms":1550097242667,"content":{"type":"text","text":{"body":"foo in team","payments":null}},"prev":null,"unread":false,"channel_mention":"none"},"pagination":{"next":"04","previous":"04","num":1,"last":false}}"#;

        let notif: Notification = serde_json::from_str(&raw_json_msg).unwrap();

        assert_eq!(notif.msg.id, 4);
        assert_eq!(notif.msg.content.assert_text().text.body, "foo in team");
    }

    #[test]
    fn json_text_from_person_is_allowed() {
        let raw_json_msg = r#"{"type":"chat","source":"remote","msg":{"id":3,"channel":{"name":"andybalaam,arnie","public":false,"members_type":"impteamnative","topic_type":"chat"},"sender":{"uid":"bbbb1111bbbb1111bbbb1111bbbb1111","username":"andybalaam","device_id":"aaaa0000aaaa0000aaaa0000aaaa0000","device_name":"Home"},"sent_at":1550097248,"sent_at_ms":1550097248537,"content":{"type":"text","text":{"body":"bar in 1-1","payments":null}},"prev":null,"unread":false,"channel_mention":"none"},"pagination":{"next":"03","previous":"03","num":1,"last":false}}"#;

        let notif: Notification = serde_json::from_str(&raw_json_msg).unwrap();

        assert_eq!(notif.msg.id, 3);
        assert_eq!(notif.msg.content.assert_text().text.body, "bar in 1-1");
    }

    #[test]
    fn process_serialises_and_deserialises() {
        let raw_json_msg = r#"{"type":"chat","source":"remote","msg":{"id":4,"channel":{"name":"arnie_and_me","public":false,"members_type":"team","topic_type":"chat","topic_name":"general"},"sender":{"uid":"bbbb1111bbbb1111bbbb1111bbbb1111","username":"andybalaam","device_id":"aaaa0000aaaa0000aaaa0000aaaa0000","device_name":"Home"},"sent_at":1550097242,"sent_at_ms":1550097242667,"content":{"type":"text","text":{"body":"!arnie Bar","payments":null}},"prev":null,"unread":false,"channel_mention":"none"},"pagination":{"next":"04","previous":"04","num":1,"last":false}}"#;

        let res = process(&cfg(), String::from(raw_json_msg)).unwrap();
        assert_eq!(
            res,
            r#"{"method":"send","params":{"options":{"channel":{"name":"arnie_and_me","members_type":"team"},"message":{"body":"*Bar*"}}}}"#
        )
    }

    #[test]
    fn team_message_from_unknown_person_is_ignored() {
        let raw_json_msg = r#"{"type":"chat","source":"remote","msg":{"id":4,"channel":{"name":"arnie_and_me","public":false,"members_type":"team","topic_type":"chat","topic_name":"general"},"sender":{"uid":"cccc1111cccc1111cccc1111cccc1111","username":"someone_else","device_id":"aaaa0000aaaa0000aaaa0000aaaa0000","device_name":"Home"},"sent_at":1550097242,"sent_at_ms":1550097242667,"content":{"type":"text","text":{"body":"!arnie Bar","payments":null}},"prev":null,"unread":false,"channel_mention":"none"},"pagination":{"next":"04","previous":"04","num":1,"last":false}}"#;
        let res = process(&cfg(), String::from(raw_json_msg)).unwrap();
        assert_eq!(res, "")
    }

    #[test]
    fn personal_message_from_unknown_person_is_ignored() {
        let raw_json_msg = r#"{"type":"chat","source":"remote","msg":{"id":3,"channel":{"name":"andybalaam,arnie","public":false,"members_type":"impteamnative","topic_type":"chat"},"sender":{"uid":"bbbb1111bbbb1111bbbb1111bbbb1111","username":"andybalaam","device_id":"aaaa0000aaaa0000aaaa0000aaaa0000","device_name":"Home"},"sent_at":1550097248,"sent_at_ms":1550097248537,"content":{"type":"text","text":{"body":"bar in 1-1","payments":null}},"prev":null,"unread":false,"channel_mention":"none"},"pagination":{"next":"03","previous":"03","num":1,"last":false}}"#;
        let res = process(&cfg(), String::from(raw_json_msg)).unwrap();
        assert_eq!(res, "")
    }

    impl Content {
        fn assert_system(&self) -> &SystemContent {
            match self {
                Content::SystemContent(sys) => sys,
                _ => panic!("Expected system type!"),
            }
        }
        fn assert_text(&self) -> &TextContent {
            match self {
                Content::TextContent(txt) => txt,
                _ => panic!("Expected text type!"),
            }
        }
    }

    fn cfg() -> Config {
        Config {
            known_usernames: vec![
                String::from("andybalaam"),
                String::from("trusted_other"),
            ],
        }
    }
}*/
