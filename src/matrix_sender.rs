use std::error::Error;

use async_trait::async_trait;
use matrix_sdk::config::SyncSettings;
use matrix_sdk::ruma::events::room::message::RoomMessageEventContent;
use matrix_sdk::ruma::{OwnedRoomId, RoomId};
use matrix_sdk::Client;
use maud::html;

use crate::args::Args;
use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::matrix_config::MatrixConfig;
use crate::message::Message;
use crate::message_sender::{MessageSender, SendError, WrongRoomAddressType};
use crate::redis_store_info::RedisStoreInfo;
use crate::room_address::RoomAddress;

pub struct MatrixSender<Db: Database> {
    client: Client,
    _db: DatabaseWrapper<Db>,
}

impl<Db: Database> MatrixSender<Db> {
    pub async fn new(
        mut db: DatabaseWrapper<Db>,
        redis_store_info: Option<RedisStoreInfo>,
        args: &Args,
    ) -> Result<Self, Box<dyn Error>> {
        // TODO: return an error when there is a missing entry in the DB.
        // TODO: later: start up fine even if missing entry and poll
        //       for good DB entries?
        let MatrixConfig {
            homeserver_url,
            username,
            password,
            store_passphrase,
        } = MatrixConfig::from_args(args, &mut db).await;

        // Device ID won't come from command line - it's only ever in Redis
        let device_id = db
            .get::<Option<String>>("sender_device_id")
            .await
            .expect("Error fetching sender_device_id");

        let client_builder = Client::builder().homeserver_url(homeserver_url);

        let client = if let Some(redis_store_info) = redis_store_info {
            client_builder
                .redis_store(
                    &redis_store_info.redis_url,
                    store_passphrase.as_deref(),
                    &redis_store_info.store_prefix,
                )
                .await?
                .build()
                .await?
        } else {
            // Otherwise, by default we use a memory store
            client_builder.build().await?
        };

        let mut login = client.login_username(username, &password);
        if let Some(device_id) = &device_id {
            login = login.device_id(device_id);
        }

        let response =
            login.initial_device_display_name("arnie").send().await?;

        db.set("sender_device_id", response.device_id.as_str())
            .await
            .expect("Error saving sender_device_id");

        // TODO: spawn a task that keeps syncing
        client.sync_once(SyncSettings::default()).await.unwrap();

        println!(
            "arnie: connected to matrix sender (device id={})",
            response.device_id.as_str()
        );

        Ok(Self { _db: db, client })
    }
}

fn find_room_id(message: &Message) -> Result<OwnedRoomId, SendError> {
    if let RoomAddress::Matrix(rid) = &message.room_address {
        RoomId::parse(rid)
            .map_err(|_| SendError::new(Box::new(WrongRoomAddressType {})))
    } else {
        Err(SendError::new(Box::new(WrongRoomAddressType {})))
    }
}

#[async_trait]
impl<Db: Database + Send> MessageSender for MatrixSender<Db> {
    // TODO: unwrap
    async fn send(&mut self, message: &Message) -> Result<(), SendError> {
        let rid = find_room_id(message)?;
        if let Some(room) = self.client.get_joined_room(&rid) {
            let text_body = format!("**{}**", message.text);
            let html_body = html! { strong { (message.text) } };
            let content =
                RoomMessageEventContent::text_html(&text_body, html_body);
            room.send(content, None).await.expect("Failed to send!");
        }

        Ok(())
    }

    fn is_addressed_by(&self, room_address: &RoomAddress) -> bool {
        matches!(room_address, RoomAddress::Matrix(_))
    }
}
