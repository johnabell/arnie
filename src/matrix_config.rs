use crate::{
    args::Args, database::Database, database_wrapper::DatabaseWrapper,
};

pub(crate) struct MatrixConfig {
    pub homeserver_url: String,
    pub username: String,
    pub password: String,
    pub store_passphrase: Option<String>,
}

impl MatrixConfig {
    pub(crate) async fn from_args<Db: Database>(
        args: &Args,
        db: &mut DatabaseWrapper<Db>,
    ) -> Self {
        let homeserver_url = fetch_required_arg(
            &args.matrix_homeserver_url,
            db,
            "homeserver_url",
            "--matrix-homeserver-url",
        )
        .await;

        let username = fetch_required_arg(
            &args.matrix_username,
            db,
            "username",
            "--matrix-username",
        )
        .await;

        let password = fetch_required_arg(
            &args.matrix_password,
            db,
            "password",
            "--matrix-password",
        )
        .await;

        let store_passphrase = fetch_optional_arg(
            &args.matrix_store_passphrase,
            db,
            "store_passphrase",
        )
        .await;

        Self {
            homeserver_url,
            username,
            password,
            store_passphrase,
        }
    }
}

async fn fetch_optional_arg<Db: Database>(
    arg: &Option<String>,
    db: &mut DatabaseWrapper<Db>,
    name: &str,
) -> Option<String> {
    // If the value was supplied on the command line or via environment
    // variable, use it, otherwise, look it up in Redis.
    if arg.is_none() {
        db.get::<Option<String>>(name).await.unwrap_or_else(|e| {
            panic!("Error fetching config entry '{}': {}", name, e)
        })
    } else {
        arg.to_owned()
    }
}

async fn fetch_required_arg<Db: Database>(
    arg: &Option<String>,
    db: &mut DatabaseWrapper<Db>,
    name: &str,
    command_line_name: &str,
) -> String {
    fetch_optional_arg(arg, db, name).await.unwrap_or_else(|| {
        panic!(
            "\
            Missing required config entry '{}'. \
            Supply it on the command line, by providing argument: {}\
            ",
            name, command_line_name
        )
    })
}
