use redis::RedisResult;

use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::error_stream::ErrorStream;
use crate::message::Message;
use crate::message_receiver::MessageReceiver;
use crate::message_receivers_list::MessageReceiversList;
use crate::message_sender::MessageSender;
use crate::plugins::admin_plugin::{AdminAction, AdminPlugin, AdminResponse};
use crate::plugins::arnie_plugin::ArniePlugin;
use crate::plugins::plugin::Plugin;
use crate::plugins::system_admin_plugin::SystemAdminPlugin;

const ERROR_CODE_ADMIN_SEND_FAILED: i32 = 14;

// TODO: list all available commands

pub struct Bot<Db: Database + Send> {
    _db: DatabaseWrapper<Db>,
    message_receivers: MessageReceiversList,
    message_senders: Vec<Box<dyn MessageSender>>,
    admin_plugins: Vec<Box<dyn AdminPlugin>>,
    plugins: Vec<Box<dyn Plugin>>,
    errors: ErrorStream,
}

impl<Db: 'static + Database + Send> Bot<Db> {
    pub async fn new(
        db: DatabaseWrapper<Db>,
        errors: ErrorStream,
        random_seed: u64,
    ) -> RedisResult<Bot<Db>> {
        let system_admin_plugin =
            Box::new(SystemAdminPlugin::new(db.namespaced("system")).await?);

        let arnie_plugin = Box::new(
            ArniePlugin::new(db.namespaced("arnie|"), random_seed).await?,
        );

        Ok(Bot {
            _db: db,
            message_receivers: MessageReceiversList::new(errors.clone()),
            message_senders: Vec::new(),
            admin_plugins: vec![system_admin_plugin],
            plugins: vec![arnie_plugin],
            errors,
        })
    }

    pub fn add_message_sender<M>(&mut self, message_sender: Box<M>)
    where
        M: MessageSender + 'static,
    {
        self.message_senders.push(message_sender);
    }

    pub fn add_message_receiver<M>(&mut self, message_receiver: M)
    where
        M: MessageReceiver + 'static,
    {
        self.message_receivers.add(message_receiver);
    }

    /**
     * Run the bot until we decide to exit.
     *
     * Returns an error indicating the status code to exit with.
     */
    pub async fn run(&mut self) -> Result<(), i32> {
        loop {
            let message = self.message_receivers.next_message().await;
            self.process(&message).await?;
        }
    }
    /**
     * Deal with a message.
     *
     * Returns an error only if the whole process should stop.
     */
    async fn process(&mut self, message: &Message) -> Result<(), i32> {
        self.process_admin_plugins(message).await?;
        self.process_plugins(message).await;
        Ok(())
    }

    /**
     * Send messages to the admin plugins and deal with their responses.
     *
     * In particular, returns an error only if the whole process
     * should stop.
     */
    async fn process_admin_plugins(
        &mut self,
        message: &Message,
    ) -> Result<(), i32> {
        // TODO: somewhere, check we are allowed to use admin plugin

        let mut responses = Vec::new();
        for admin_plugin in self.admin_plugins.iter_mut() {
            responses.push(admin_plugin.message(message).await);
        }

        for res in responses {
            self.process_admin_response(message, res).await?;
        }
        Ok(())
    }

    async fn process_admin_response(
        &mut self,
        message: &Message,
        res: RedisResult<AdminResponse>,
    ) -> Result<(), i32> {
        match res {
            Ok(AdminResponse {
                action: AdminAction::Exit(code),
                message: response,
            }) => {
                self.handle_admin_response_message(message, response)
                    .await?;
                Err(code)
            }
            Ok(AdminResponse {
                action: AdminAction::Error(module, error_message),
                message: response,
            }) => {
                self.handle_admin_response_message(message, response)
                    .await?;
                self.errors.send(&module, &error_message);
                Ok(())
            }
            Ok(AdminResponse {
                action: AdminAction::None,
                message: response,
            }) => {
                self.handle_admin_response_message(message, response)
                    .await?;
                Ok(())
            }
            Err(e) => {
                // Log the error, but don't quit the program
                self.errors
                    .send("bot::process_admin_plugins", &e.to_string());
                Ok(())
            }
        }
    }

    async fn handle_admin_response_message(
        &mut self,
        _original_message: &Message,
        response: Option<Message>,
    ) -> Result<(), i32> {
        if let Some(response) = response {
            let mut errors = Vec::new();
            for message_sender in self.message_senders.iter_mut() {
                let send_res = message_sender.send(&response).await;
                if let Err(error) = send_res {
                    errors.push(error.to_string());
                };
                // TODO: send message to sender instead of all
            }
            if !errors.is_empty() {
                for error in errors {
                    self.errors.send(
                        "bot::process_admin_response",
                        &error.to_string(),
                    );
                }
                return Err(ERROR_CODE_ADMIN_SEND_FAILED);
            }
        }
        Ok(())
    }

    /**
     * Send messages to the plugins and deal with their responses.
     */
    async fn process_plugins(&mut self, message: &Message) {
        for plugin in self.plugins.iter_mut() {
            let res = plugin.message(message).await;
            match res {
                Ok(Some(response)) => {
                    for message_sender in self
                        .message_senders
                        .iter_mut()
                        .filter(|message_sender| {
                            message_sender
                                .is_addressed_by(&response.room_address)
                        })
                    {
                        message_sender.send(&response).await.unwrap();
                    }
                }
                Ok(None) => {} // No message to send, just continue
                Err(e) => {
                    // TODO: surface an error
                    println!("Error from plugin: {e}");
                }
            }
        }
    }
}

#[cfg(test)]
mod test {
    use async_trait::async_trait;
    use std::sync::Arc;
    use tokio::sync::Mutex;

    use crate::{
        command_line_sender::CommandLineSender,
        memory_database::MemoryDatabase, message_sender::SendError,
        room_address::RoomAddress, write_buffer::WriteBuffer,
    };

    use super::*;

    struct FakeMatrixSender {}

    #[async_trait]
    impl MessageSender for FakeMatrixSender {
        async fn send(&mut self, _message: &Message) -> Result<(), SendError> {
            todo!("No messages should be sent to the FakeMatrixSender in this test");
        }

        fn is_addressed_by(&self, room_address: &RoomAddress) -> bool {
            match room_address {
                RoomAddress::Matrix(_) => true,
                _ => false,
            }
        }
    }

    struct EchoPlugin {}

    impl EchoPlugin {
        fn new() -> Self {
            Self {}
        }
    }

    #[async_trait]
    impl Plugin for EchoPlugin {
        async fn message(
            &mut self,
            message: &Message,
        ) -> RedisResult<Option<Message>> {
            Ok(Some(message.clone()))
        }
    }

    #[tokio::test]
    async fn messages_go_to_the_right_sender() {
        let stdout = WriteBuffer::new();
        let stderr = WriteBuffer::new();

        let mem_db = DatabaseWrapper::new(
            Arc::new(Mutex::new(MemoryDatabase::new())),
            String::from(""),
        );
        let mut bot = Bot::new(mem_db, ErrorStream::new(stderr.clone()), 1)
            .await
            .expect("Failed to create bot");

        bot.add_message_sender(Box::new(
            CommandLineSender::new(stdout.clone()).unwrap(),
        ));
        bot.add_message_sender(Box::new(FakeMatrixSender {}));

        bot.plugins.push(Box::new(EchoPlugin::new()));

        let incoming_message =
            Message::new("hello echo".to_owned(), RoomAddress::CommandLine);
        bot.process(&incoming_message).await.unwrap();

        assert_eq!(stdout.to_string(), ">>> hello echo\n>>> ");

        // We know the FakeMatrixSender wasn't sent it because it will
        // panic if it is sent a message.
    }
}
