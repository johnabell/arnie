use std::error::Error;
use std::time::Duration;

use async_trait::async_trait;
use matrix_sdk::config::SyncSettings;
use matrix_sdk::room::Room;
use matrix_sdk::ruma::events::room::message::{
    MessageType, OriginalSyncRoomMessageEvent,
};
use matrix_sdk::Client;
use tokio::sync::mpsc;
use tokio::time::{sleep, Instant};

use crate::args::Args;
use crate::database::Database;
use crate::database_wrapper::DatabaseWrapper;
use crate::matrix_config::MatrixConfig;
use crate::message::Message;
use crate::message_receiver::MessageReceiver;
use crate::redis_store_info::RedisStoreInfo;
use crate::room_address::RoomAddress;

/// We refuse to sync again until at least this long since the last time
/// we synced.
const MIN_TIME_BETWEEN_SYNCS: Duration = Duration::from_secs(30);

pub struct MatrixReceiver<Db: Database> {
    client: Client,
    _db: DatabaseWrapper<Db>,
}

impl<Db: Database> MatrixReceiver<Db> {
    pub async fn new(
        mut db: DatabaseWrapper<Db>,
        redis_store_info: Option<RedisStoreInfo>,
        args: &Args,
    ) -> Result<Self, Box<dyn Error>> {
        // TODO: return an error when there is a missing entry in the DB.
        // TODO: later: start up fine even if missing entry and poll
        //       for good DB entries?
        let MatrixConfig {
            homeserver_url,
            username,
            password,
            store_passphrase,
        } = MatrixConfig::from_args(args, &mut db).await;

        let device_id = db
            .get::<Option<String>>("receiver_device_id")
            .await
            .expect("Error fetching receiver_device_id");

        let client_builder = Client::builder().homeserver_url(homeserver_url);

        let client = if let Some(redis_store_info) = redis_store_info {
            client_builder
                .redis_store(
                    &redis_store_info.redis_url,
                    store_passphrase.as_deref(),
                    &redis_store_info.store_prefix,
                )
                .await?
                .build()
                .await?
        } else {
            // Otherwise, by default we use a memory store
            client_builder.build().await?
        };

        let mut login = client.login_username(username, &password);
        if let Some(device_id) = &device_id {
            login = login.device_id(device_id);
        }

        let response = login
            .initial_device_display_name("arnie")
            .send()
            .await
            .unwrap();

        db.set("receiver_device_id", response.device_id.as_str())
            .await
            .expect("Error saving receiver_device_id");

        println!(
            "arnie: connected to matrix receiver (device id={})",
            response.device_id.as_str()
        );

        Ok(Self { _db: db, client })
    }
}

#[async_trait]
impl<Db: Database + Send> MessageReceiver for MatrixReceiver<Db> {
    async fn receive_forever(self, sender: mpsc::Sender<Message>) {
        // We only want the bot to see messages that come after it starts, so
        // do a sync here to bypass any messages that have already been sent.
        // TODO: unwraps
        self.client
            .sync_once(SyncSettings::default())
            .await
            .unwrap();

        self.client.add_event_handler(
            move |event: OriginalSyncRoomMessageEvent, room: Room| {
                let event_id = event.event_id.to_owned();
                let sender = sender.clone();
                async move {
                    if let (
                        Room::Joined(room),
                        MessageType::Text(text_content),
                    ) = (room, event.content.msgtype)
                    {
                        let msg = Message::new(
                            text_content.body,
                            RoomAddress::Matrix(room.room_id().to_string()),
                        );

                        // Pass the message we received back to the bot
                        // TODO: unwraps
                        sender
                            .send(msg)
                            .await
                            .expect("Unable to pass message on to bot");

                        // Send a read receipt
                        room.read_receipt(&event_id)
                            .await
                            .expect("Unable send read receipt");
                    }
                }
            },
        );

        loop {
            let sync_start = Instant::now();
            let sync_result = self.client.sync(SyncSettings::default()).await;
            if let Err(e) = sync_result {
                // TODO: surface error
                println!("Sync returned with error: {e}");

                // Sleep until a reasonable time since the last sync started
                let since_last_sync = sync_start.elapsed();
                if since_last_sync < MIN_TIME_BETWEEN_SYNCS {
                    let sleep_time = MIN_TIME_BETWEEN_SYNCS - since_last_sync;
                    // TODO: surface message
                    println!(
                        "Waiting for a {}s gap between syncs.",
                        MIN_TIME_BETWEEN_SYNCS.as_secs()
                    );
                    sleep(sleep_time).await;
                }
            }
            // sync will never return Ok, so no else.
        }
    }
}
